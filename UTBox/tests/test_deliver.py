import unittest

from unittest.mock import patch

from UTBox.exceptions import FolderError
import UTBox.deliver as deliver


class TestDeliver(unittest.TestCase):

    # def test_get_box_client_oauth(self):
    #     self.assertEqual(False, True)

    def test_store_tokens(self):
        filepath = 'UTBox/tests/test_data/test_update_tokens.json'
        expected_access = 'access!'
        expected_refresh = 'refresh!'

        deliver.store_tokens(expected_access, expected_refresh, filepath)

        test_result = deliver.read_tokens(filepath)

        self.assertEqual((expected_access, expected_refresh), test_result)

    def test_read_tokens(self):
        filename = 'UTBox/tests/test_data/test_tokens.json'
        expected = ('access_value', 'refresh_value')
        actual = deliver.read_tokens(filename)
        self.assertEqual(expected, actual)

    # def test_get_folder(self):
    #     self.assertEqual(False, True)
    #
    # def test_get_reports_folder(self):
    #     self.assertEqual(False, True)
    #
    # def test_get_reports_folder_error(self):
    #     self.assertEqual(False, True)
    #
    # def test_upload_report(self):
    #     self.assertEqual(False, True)
    #
    # def test_upload_report_error(self):
    #     self.assertEqual(False, True)
