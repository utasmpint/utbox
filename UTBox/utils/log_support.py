import os
import logging.config
import yaml

from UTBox.config import PROJ_CONFIG


def setup_logging(
    default_path='project_config.yaml',
    default_level=logging.INFO,
    env_key=PROJ_CONFIG
):
    """Setup logging configuration

    """

    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config['LOGGING'])
    else:
        logging.basicConfig(level=default_level)
