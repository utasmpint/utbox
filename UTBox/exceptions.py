class ConfigurationError(Exception):
    pass


class PathError(Exception):
    pass


class FolderError(Exception):
    pass


class EmailError(Exception):
    pass
