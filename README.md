# README #

This project supports using UT Box

### What is this repository for? ###

* Summary: This project supports using UT Box to programatically deliver 
reports to given Box accounts and folders.  It is designed to be using directly
in your own application or can be used free form as a Stonebranch Linux task.
* Version: 1.0

### How do I get set up? ###

* Check out this repository

* Get a virtual environment (e.g., pyvenv) set up and pip install the
  dev_requirements file once the virtualenv has been activated.
  
* Set up an environment variable called ASMP_ENV that points to the environment 
you'll be using - probably LOCAL.

* Database configuration
    * None


* How to run tests
    * make sure dev_requirements have been installed
    * run nosetests


* Deployment instructions
    * [Manual Deployment](https://wikis.utexas.edu/display/asmperpint/Manually)
    * [Deployment with Ansible](https://wikis.utexas.edu/display/asmperpint/With+Ansible)

### Access Tokens and Refresh Tokens ###

* Access Tokens are good for approx. 60 minutes
* Refresh Tokens are good for 60 days
* As long as something is using the account at least once every 60 days we
never need to manually intervene
* If the account is inactive for more than 60 days, we'll have to manually
refresh the access and refresh token.  This is because of how Box is set up
and that it requires a user to log in to a web page.
* How To Manually Generate Tokens
** Go To the following URl with your client id filled in

```
https://app.box.com/api/oauth2/authorize?response_type=code&client_id={client_id}&from=box&from=box
```

** Log In with the departmental user creds (avalable in Stache)
** Response URL will have the Access Code in it

```
https://localhost/?code={access code}
```

** Access Code is only valid for about 30 seconds so you have to act quickly
** In a terminal do the following:

```
curl https://api.box.com/oauth2/token \
-d 'grant_type=authorization_code&code={access code from previous step}&client_id={client_id}&client_secret={client_secret}' \
-X POST
```

** Copy the refresh token and access token from the response back into the token file on the server


### Functionality ###

* Will always upload as ai4558 user.
* Folder to upload files to must already exist in UT Box
* Folders need to be created by ai4558 but then ownership can be passed off to a department.
* ai4558 must have at least Editor auth level to be able to upload new files to the folder.
* File name must be unique - will not overwrite files

* Can be installed via requirements into an existing project and called directly
* Can be used via command line utilties (and therefore Stonebranch tasks)
** python start.py {file to be uploaded to Box}
                   {UTBox Folder within ai5448 account to upload to}

### Contribution guidelines ###

You are expected to write tests for any new functionality you add.  All tests 
should pass and coverage shoudl remain above 80%.

### Who do I talk to? ###

* [ASMP Integrations Tech Team](mailto:asmp-erp.integrations-tech@austin.utexas.edu)